**Atelier Scenari ScenariStyler 6**

**1. Installation du modèle documentaire ScenariStyler.**
Lorsqu'on créé un atelier ScenariStyler, il faut choisir l'extension correspondant au modèle documentaire (ici, Styler IDkey 2). Il est nécessaire de créer 2 fichiers : Jeu d'habillage graphique (fichier .skinset) et Habillage graphique par formulaire (fichier .skin)

**2. Exemples de fichiers créés**
Fichier "ariane.skinset" :
Contient l'identifiant, l'icône et l'habillage graphique (possibilité d'en mettre plusieurs)

Fichier "site-web.skin" : 
fichier .skin est l'habillage graphique
Contient les règles de style spécifiques appliquées (par exemple, le choix de la police, la couleur, la largeur de notice, etc)

Fichier "logo-ariane.png" : 
Image du logo ariane 

**3. L'association à un modèle documentaire existant**
L'association des deux modèles se réalise sur le fichier "inventaire_projet.idkey" au niveau de la génération web (sous le sigle "Site web (PWA)")

L'habillage créé peut fonctionner sur tous les ateliers compatibles présents en local.